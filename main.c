#include "main.h"


int main( void ) {

	FILE* in;
	FILE* out;
	struct image* target = ( struct image* ) malloc( sizeof( struct image ) );
	char* input_file = "./image2.bmp" + 0;
	char* output_file = "./newImage.bmp" + 0;
	enum read_status read_status = ( enum read_status ) malloc( sizeof( enum read_status ) );
	enum write_status write_status = ( enum write_status ) malloc( sizeof( enum write_status ) );

	in = fopen( input_file, "rb" );

	if( in == NULL ) {
		printf( "Can't open input file with name %s", input_file );
		return 0;
	}

	read_status = read_bmp( in, target );
	print_read_status( read_status );

	if( read_status == READ_OK ) {
		out = fopen( output_file, "wb" );
		if( in == NULL ) {
			printf( "Can't open output file with name %s", output_file );
			fclose( in );
			return 0;
		}
		target = rotate_image( target, 90 );
		write_status = write_bmp( out, target );
		print_write_status( write_status );
	}

	fclose( in );
	fclose( out );
	return 1;
}
