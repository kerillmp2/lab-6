#ifndef _BMP_IO_H_
#define _BMP_IO_H_

#include <stdint.h>
#include <stdio.h>
#include <math.h>
#include <malloc.h>

#include "bmp_enums.h"
#include "bmp_image.h"

enum read_status read_bmp( FILE*, struct image* );
enum write_status write_bmp( FILE*, struct image* );
void print_read_status( enum read_status );
void print_write_status( enum write_status );

#endif
