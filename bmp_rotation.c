#include "bmp_rotation.h"

struct image* flip_image( struct image* const target ) {
	uint32_t width = target->width;
	uint32_t height = target->height;
	struct pixel* bits = target->data;
	struct pixel* new_bits = ( struct pixel* ) malloc( width * height * sizeof( struct pixel ) );
	size_t i, j;

	for( i = 0; i < height; i++ ) {
		for( j = 0; j < width; j++ ) {
			*( new_bits + ( height - i - 1 ) * width + ( width - j - 1) ) = *( bits + i * width + j );
		}
	}

	target->data = new_bits;
	free( bits );
	return target;
}

struct image* rotate_image( struct image* const target, int32_t rotator ) {

	double angle;
	uint32_t width = target->width;
	uint32_t height = target->height;
	struct pixel* bits = target->data;
	struct pixel* new_bits = ( struct pixel* ) malloc( width * height * sizeof( struct pixel ) );
	size_t i, j;
	int32_t prev_x;
	int32_t prev_y;
	uint32_t new_x;
	uint32_t new_y;

	rotator = rotator % 360;
	if ( rotator < 0 ) {
		rotator = 360 + rotator;
	}
	angle = rotator * M_PI / 180;


	if( rotator % 180 == 0 ) {
		if( rotator % 360 == 0 ) {
			free( new_bits );
			return target;
		}
		free( new_bits );
		return flip_image( target );
	}

	if( rotator != 90 ) {
		printf( "Angle must be 90,  or 360*n + 90. Source image returned.\n" );
		return target;
	}

	for( i = 0; i < height; i++ ) {
		for( j = 0; j < width; j++ ) {
			prev_x = j;
			prev_y = i;
			new_x = prev_x * ( cos( angle ) ) + prev_y * ( sin( angle ) );
			new_y = prev_x * ( sin( angle ) ) + prev_y * ( cos( angle ) );

			/* printf("px: %d, py: %d, nx: %d, ny: %d\n", prev_x, prev_y, new_x, new_y);
			printf("old_bit: %d\n", i * width + j);
			printf("new_bit: %d\n", new_y*height + new_x);
			printf("i: %d; j: %d\n", i, j);
			printf("---------------------\n"); */

			*( new_bits + new_y * height + new_x ) = *( bits + i * width + j );
		}
	}

	target->width = height;
	target->height = width;
	target->data = new_bits;

	if( width > height ) {
		free( bits );
		return flip_image( target );
	}

	free( bits );
	return target;
}
