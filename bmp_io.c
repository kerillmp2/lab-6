#include "bmp_io.h"

enum read_status read_bmp( FILE* in, struct image* target ) {
	struct bmp_header* header = ( struct bmp_header* ) malloc( sizeof( struct bmp_header ) );
	struct pixel* bits;
	uint64_t offset;
	uint8_t spaces[4];
	size_t i;

	if( !fread( header, sizeof( struct bmp_header ), 1, in )) {
		return READ_ERROR;
	}

	fseek( in, header->bOffBits, SEEK_SET );

	if( header->bfType != 0x4D42 || header->biBitCount != 24 ) {
		return READ_HEADER_ERROR;
	}

	target->width = header->biWidth;
	target->height = header->biHeight;
	offset = calculate_offset( target->width );

	bits = ( struct pixel* ) malloc( target->height * target->width * sizeof( struct pixel ) );
	for( i = 0; i < target->height; i++ ) {
		if ( !fread( bits + i * target->width, sizeof( struct pixel ), target->width, in ) ) {
			return READ_ERROR;
		}
		if ( offset != 0 ) {
			fread( spaces, offset, 1, in );
		}
	}

	target->data = bits;
	free(header);
	return READ_OK;
}

enum write_status write_bmp( FILE* out, struct image* target ) {
	uint8_t spaces[4] = { 0 };
	struct bmp_header header;
	size_t offset = calculate_offset( target->width );
	size_t i;

	header = create_bmp_header( target );

	if( !fwrite( &header, sizeof( struct bmp_header ), 1, out ) ) {
		return WRITE_ERROR;
	}

	for( i = 0; i < target->height; i++ ) {
		if( !fwrite( &target->data[ target->width * i ], sizeof( struct pixel ), target->width, out ) ) {
			return WRITE_ERROR;
		}

		if( offset != 0 ) {
			if( !fwrite( spaces, sizeof( uint8_t ), offset, out ) ) {
				return WRITE_ERROR;
			}
		}
	}

	return WRITE_OK;
}

void print_read_status( enum read_status read_status ) {
	switch( read_status ) {
		case READ_OK:
			printf("Reading was successful\n");
			break;
		case READ_HEADER_ERROR:
			printf("Header of bmp file is invalid\n");
			break;
		case READ_ERROR:
			printf("Error on reading data from bmp file\n");
			break;
	}
}

void print_write_status( enum write_status write_status ) {
	switch( write_status ) {
		case WRITE_OK:
			printf("Writing was successful\n");
			break;
		case WRITE_ERROR:
			printf("Error on writing data in file\n");
			break;
	}
}
