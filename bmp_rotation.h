#ifndef _BMP_ROTATION_H_
#define _BMP_ROTATION_H_

#include <stdint.h>
#include <math.h>
#include <stdio.h>
#include <malloc.h>

#ifndef M_PI
#define M_PI acos(-1)
#endif

struct image* flip_image( struct image* const );
struct image* rotate_image( struct image* const, int32_t );

#endif
