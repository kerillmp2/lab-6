#ifndef _MAIN_H_
#define _MAIN_H_

#include <stdio.h>
#include <malloc.h>

#ifndef M_PI
#define M_PI acos(-1)
#endif

#include "bmp_enums.c"
#include "bmp_io.c"
#include "bmp_image.c"
#include "bmp_rotation.c"

#endif
